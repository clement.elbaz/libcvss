#!/bin/bash

# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the MIT License, <LICENSE or http://opensource.org/licenses/MIT>.
# This file may not be copied, modified, or distributed except according to those terms.

#
# Pre-commit hook for the tarpc repository. To use this hook, copy it to .git/hooks in your
# repository root.
#
# This precommit checks the following:
# 1. All filenames are ascii
# 2. rustfmt is installed
# 3. rustfmt is a noop on files that are in the index
# 4. Tests are passing and no warnings in the code
#
# Options:
#
# - TARPC_SKIP_RUSTFMT, default = 0
#
#   Set this to 1 to skip running rustfmt
#
# Note that these options are most useful for testing the hooks themselves. Use git commit
# --no-verify to skip the pre-commit hook altogether.

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

PREFIX="${GREEN}[PRECOMMIT]${NC}"
FAILURE="${RED}FAILED${NC}"
WARNING="${RED}[WARNING]${NC}"
SKIPPED="${YELLOW}SKIPPED${NC}"
SUCCESS="${GREEN}ok${NC}"

if git rev-parse --verify HEAD &>/dev/null
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

FAILED=0

printf "${PREFIX} Checking that all filenames are ascii ... "
# Note that the use of brackets around a tr range is ok here, (it's
# even required, for portability to Solaris 10's /usr/bin/tr), since
# the square bracket bytes happen to fall in the designated range.
if test $(git diff --cached --name-only --diff-filter=A -z $against | LC_ALL=C tr -d '[ -~]\0' | wc -c) != 0
then
	FAILED=1
	printf "${FAILURE}\n"
else
	printf "${SUCCESS}\n"
fi

printf "${PREFIX} Checking for rustfmt ... "
command -v cargo fmt &>/dev/null
if [ $? == 0 ]; then
	printf "${SUCCESS}\n"
else
	printf "${FAILURE}\n"
	exit 1
fi

printf "${PREFIX} Checking for shasum ... "
command -v shasum &>/dev/null
if [ $? == 0 ]; then
	printf "${SUCCESS}\n"
else
	printf "${FAILURE}\n"
	exit 1
fi

# Just check that running rustfmt doesn't do anything to the file. I do this instead of
# modifying the file because I don't want to mess with the developer's index, which may
# not only contain discrete files.
printf "${PREFIX} Checking formatting ... "
cargo fmt --all -- --check
FMTRESULT=$?

if [ "${TARPC_SKIP_RUSTFMT}" == 1 ]; then
	printf "${SKIPPED}\n"$?
elif [ ${FMTRESULT} != 0 ]; then
	FAILED=1
	printf "${FAILURE}\n"
    echo "$diff" | sed 's/Using rustfmt config file.*$/d/'
else
	printf "${SUCCESS}\n"
fi

printf "${PREFIX} Checking for test and warnings ... "
cargo test --features=fail-on-warnings &>/dev/null
if [ $? == 0 ]; then
	printf "${SUCCESS}\n"
else
	printf "${FAILURE}\n"
	exit 1
fi

exit ${FAILED}
