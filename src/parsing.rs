use std::str::Split;

pub trait Parser: Sized {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized;

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized;
}

pub fn parse_strict_default_field<T: Field>(
    split: &mut Box<Split<char>>,
) -> Result<T, Vec<&'static str>> {
    match split.next() {
        None => Err(vec![T::error_message()]),
        Some(token) => match T::parse(token) {
            None => Err(vec![T::error_message()]),
            Some(res) => Ok(res),
        },
    }
}

pub fn parse_nonstrict_default_field<T: Field>(
    split: &Box<Split<char>>,
) -> Result<T, Vec<&'static str>> {
    let cloned_split = split.clone();

    match cloned_split.filter_map(T::parse).next() {
        None => Err(vec![T::error_message()]),
        Some(res) => Ok(res),
    }
}

pub trait OptionalParser: Sized {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized;
}

pub fn parse_optional_default_field<T: Field>(split: &Box<Split<char>>) -> Option<T> {
    let cloned_split = split.clone();
    cloned_split.filter_map(T::parse).next()
}

pub trait Field {
    fn abbreviated_form(&self) -> &'static str;

    fn error_message() -> &'static str;

    fn parse(input: &str) -> Option<Self>
    where
        Self: Sized;
}
