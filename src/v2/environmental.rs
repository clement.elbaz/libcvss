//! Environmental vector implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use crate::v2::base::BaseVector;
use crate::v2::temporal::TemporalVector;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// A CVSS V2.0 environmental vector.
///
/// Per the CVSS specification, this structure contains a Collateral Damage Potential field, a Target Distribution field, a Confidentiality Requirement field, an Integrity Requirement field, and an Availability Requirement field, all optionals.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct EnvironmentalVector {
    /// Optional Collateral Damage Potential field as defined by the CVSS specification.
    pub collateral_damage_potential: Option<CollateralDamagePotential>,
    /// Optional Target Distribution field as defined by the CVSS specification.
    pub target_distribution: Option<TargetDistribution>,
    /// Optional Confidentiality Requirement field as defined by the CVSS specification.
    pub confidentiality_requirement: Option<ConfidentialityRequirement>,
    /// Optional Integrity Requirement field as defined by the CVSS specification.
    pub integrity_requirement: Option<IntegrityRequirement>,
    /// Optional Availability Requirement field as defined by the CVSS specification.
    pub availability_requirement: Option<AvailabilityRequirement>,
}

/// The Collateral Damage Potential field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`EnvironmentalVector.collateral_damage_potential`] to [`None`].
///
/// [`EnvironmentalVector.collateral_damage_potential`]: struct.EnvironmentalVector.html#structfield.collateral_damage_potential
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum CollateralDamagePotential {
    /// None ("CDP:N") value for the Collateral Damage Potential field.
    None,
    /// Low ("CDP:L") value for the Collateral Damage Potential field.
    Low,
    /// Low Medium ("CDP:LM") value for the Collateral Damage Potential field.
    LowMedium,
    /// Medium High ("CDP:MH") value for the Collateral Damage Potential field.
    MediumHigh,
    /// High ("CDP:H") value for the Collateral Damage Potential field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting EnvironmentalVector.collateral_damage_potential to None.
}

/// The Target Distribution field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`EnvironmentalVector.target_distribution`] to [`None`].
///
/// [`EnvironmentalVector.target_distribution`]: struct.EnvironmentalVector.html#structfield.target_distribution
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum TargetDistribution {
    /// None ("TD:N") value for the Target Distribution field.
    None,
    /// Low ("TD:L") value for the Target Distribution field.
    Low,
    /// Medium ("TD:M") value for the Target Distribution field.
    Medium,
    /// High ("TD:H") value for the Target Distribution field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting EnvironmentalVector.target_distribution to None.
}

/// The Confidentiality Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`EnvironmentalVector.confidentiality_requirement`] to [`None`].
///
/// [`EnvironmentalVector.confidentiality_requirement`]: struct.EnvironmentalVector.html#structfield.confidentiality_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ConfidentialityRequirement {
    /// Low ("CR:L") value for the Confidentiality Requirement field.
    Low,
    /// Medium ("CR:M") value for the Confidentiality Requirement field.
    Medium,
    /// High ("CR:H") value for the Confidentiality Requirement field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting EnvironmentalVector.confidentiality_requirement to None.
}

/// The Integrity Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`EnvironmentalVector.integrity_requirement`] to [`None`].
///
/// [`EnvironmentalVector.integrity_requirement`]: struct.EnvironmentalVector.html#structfield.integrity_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum IntegrityRequirement {
    /// Low ("IR:L") value for the Integrity Requirement field.
    Low,
    /// Medium ("IR:M") value for the Integrity Requirement field.
    Medium,
    /// High ("IR:H") value for the Integrity Requirement field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting EnvironmentalVector.integrity_requirement to None.
}

/// The Availability Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`EnvironmentalVector.availability_requirement`] to [`None`].
///
/// [`EnvironmentalVector.availability_requirement`]: struct.EnvironmentalVector.html#structfield.availability_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum AvailabilityRequirement {
    /// Low ("AR:L") value for the Availability Requirement field.
    Low,
    /// Medium ("AR:M") value for the Availability Requirement field.
    Medium,
    /// High ("AR:H") value for the Availability Requirement field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting EnvironmentalVector.availability_requirement to None.
}

impl EnvironmentalVector {
    /// Provides the severity score for the CVSS environmental vector, given a mandatory base vector and an optional environmental vector.
    ///
    /// Calling this method is identical to calling [`CVSS2Vector.score()`].
    ///
    /// [`CVSS2Vector.score()`]: ../struct.CVSS2Vector.html#method.score
    pub fn score(&self, base_vector: BaseVector, temporal_vector: Option<TemporalVector>) -> f64 {
        let confidentiality_requirement = match self.confidentiality_requirement {
            None => 1.0,
            Some(requirement) => requirement.numerical_value(),
        };

        let integrity_requirement = match self.integrity_requirement {
            None => 1.0,
            Some(requirement) => requirement.numerical_value(),
        };

        let availability_requirement = match self.availability_requirement {
            None => 1.0,
            Some(requirement) => requirement.numerical_value(),
        };

        let adjusted_impact = f64::min(
            10.0,
            10.41
                * (1.0
                    - (1.0
                        - base_vector.confidentiality_impact.numerical_value()
                            * confidentiality_requirement)
                        * (1.0
                            - base_vector.integrity_impact.numerical_value()
                                * integrity_requirement)
                        * (1.0
                            - base_vector.availability_impact.numerical_value()
                                * availability_requirement)),
        );

        let adjusted_base = base_vector.score_with_adjusted_impact(adjusted_impact);

        let adjusted_temporal = match temporal_vector {
            None => adjusted_base,
            Some(vector) => vector.score(adjusted_base),
        };

        let collateral_damage_potential = match self.collateral_damage_potential {
            None => 0.0,
            Some(cdp) => cdp.numerical_value(),
        };

        let target_distribution = match self.target_distribution {
            None => 1.0,
            Some(td) => td.numerical_value(),
        };

        (10.0
            * (adjusted_temporal + (10.0 - adjusted_temporal) * collateral_damage_potential)
            * target_distribution)
            .round()
            / 10.0
    }
}

impl CollateralDamagePotential {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            CollateralDamagePotential::None => 0.0,
            CollateralDamagePotential::Low => 0.1,
            CollateralDamagePotential::LowMedium => 0.3,
            CollateralDamagePotential::MediumHigh => 0.4,
            CollateralDamagePotential::High => 0.5,
        }
    }
}

impl TargetDistribution {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            TargetDistribution::None => 0.0,
            TargetDistribution::Low => 0.25,
            TargetDistribution::Medium => 0.75,
            TargetDistribution::High => 1.0,
        }
    }
}

impl ConfidentialityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ConfidentialityRequirement::Low => 0.5,
            ConfidentialityRequirement::Medium => 1.0,
            ConfidentialityRequirement::High => 1.51,
        }
    }
}

impl IntegrityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            IntegrityRequirement::Low => 0.5,
            IntegrityRequirement::Medium => 1.0,
            IntegrityRequirement::High => 1.51,
        }
    }
}

impl AvailabilityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            AvailabilityRequirement::Low => 0.5,
            AvailabilityRequirement::Medium => 1.0,
            AvailabilityRequirement::High => 1.51,
        }
    }
}

impl OptionalParser for EnvironmentalVector {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = EnvironmentalVector {
            collateral_damage_potential: CollateralDamagePotential::parse_optional(split),
            target_distribution: TargetDistribution::parse_optional(split),
            confidentiality_requirement: ConfidentialityRequirement::parse_optional(split),
            integrity_requirement: IntegrityRequirement::parse_optional(split),
            availability_requirement: AvailabilityRequirement::parse_optional(split),
        };

        if res.collateral_damage_potential.is_none()
            && res.target_distribution.is_none()
            && res.confidentiality_requirement.is_none()
            && res.integrity_requirement.is_none()
            && res.availability_requirement.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl Field for CollateralDamagePotential {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            CollateralDamagePotential::None => "CDP:N",
            CollateralDamagePotential::Low => "CDP:L",
            CollateralDamagePotential::LowMedium => "CDP:LM",
            CollateralDamagePotential::MediumHigh => "CDP:MH",
            CollateralDamagePotential::High => "CDP:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<CollateralDamagePotential> {
        if input == CollateralDamagePotential::None.abbreviated_form() {
            Some(CollateralDamagePotential::None)
        } else if input == CollateralDamagePotential::Low.abbreviated_form() {
            Some(CollateralDamagePotential::Low)
        } else if input == CollateralDamagePotential::LowMedium.abbreviated_form() {
            Some(CollateralDamagePotential::LowMedium)
        } else if input == CollateralDamagePotential::MediumHigh.abbreviated_form() {
            Some(CollateralDamagePotential::MediumHigh)
        } else if input == CollateralDamagePotential::High.abbreviated_form() {
            Some(CollateralDamagePotential::High)
        } else {
            None
        }
    }
}

impl Field for TargetDistribution {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            TargetDistribution::None => "TD:N",
            TargetDistribution::Low => "TD:L",
            TargetDistribution::Medium => "TD:M",
            TargetDistribution::High => "TD:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<TargetDistribution> {
        if input == TargetDistribution::None.abbreviated_form() {
            Some(TargetDistribution::None)
        } else if input == TargetDistribution::Low.abbreviated_form() {
            Some(TargetDistribution::Low)
        } else if input == TargetDistribution::Medium.abbreviated_form() {
            Some(TargetDistribution::Medium)
        } else if input == TargetDistribution::High.abbreviated_form() {
            Some(TargetDistribution::High)
        } else {
            None
        }
    }
}

impl Field for ConfidentialityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ConfidentialityRequirement::Low => "CR:L",
            ConfidentialityRequirement::Medium => "CR:M",
            ConfidentialityRequirement::High => "CR:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ConfidentialityRequirement> {
        if input == ConfidentialityRequirement::Low.abbreviated_form() {
            Some(ConfidentialityRequirement::Low)
        } else if input == ConfidentialityRequirement::Medium.abbreviated_form() {
            Some(ConfidentialityRequirement::Medium)
        } else if input == ConfidentialityRequirement::High.abbreviated_form() {
            Some(ConfidentialityRequirement::High)
        } else {
            None
        }
    }
}

impl Field for IntegrityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            IntegrityRequirement::Low => "IR:L",
            IntegrityRequirement::Medium => "IR:M",
            IntegrityRequirement::High => "IR:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<IntegrityRequirement> {
        if input == IntegrityRequirement::Low.abbreviated_form() {
            Some(IntegrityRequirement::Low)
        } else if input == IntegrityRequirement::Medium.abbreviated_form() {
            Some(IntegrityRequirement::Medium)
        } else if input == IntegrityRequirement::High.abbreviated_form() {
            Some(IntegrityRequirement::High)
        } else {
            None
        }
    }
}

impl Field for AvailabilityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            AvailabilityRequirement::Low => "AR:L",
            AvailabilityRequirement::Medium => "AR:M",
            AvailabilityRequirement::High => "AR:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<AvailabilityRequirement> {
        if input == AvailabilityRequirement::Low.abbreviated_form() {
            Some(AvailabilityRequirement::Low)
        } else if input == AvailabilityRequirement::Medium.abbreviated_form() {
            Some(AvailabilityRequirement::Medium)
        } else if input == AvailabilityRequirement::High.abbreviated_form() {
            Some(AvailabilityRequirement::High)
        } else {
            None
        }
    }
}

impl OptionalParser for CollateralDamagePotential {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for TargetDistribution {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for ConfidentialityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for IntegrityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for AvailabilityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for CollateralDamagePotential {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for TargetDistribution {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ConfidentialityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for IntegrityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for AvailabilityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for EnvironmentalVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.collateral_damage_potential {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.target_distribution {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.confidentiality_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.integrity_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.availability_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::v2::base::{
        AccessComplexity, AccessVector, Authentication, AvailabilityImpact, BaseVector,
        ConfidentialityImpact, IntegrityImpact,
    };
    use crate::v2::temporal::{Exploitability, RemediationLevel, ReportConfidence, TemporalVector};

    #[test]
    fn test_formatting() {
        // Collateral Damage Potential
        assert_eq!("CDP:N", format!("{}", CollateralDamagePotential::None));
        assert_eq!("CDP:L", format!("{}", CollateralDamagePotential::Low));
        assert_eq!(
            "CDP:LM",
            format!("{}", CollateralDamagePotential::LowMedium)
        );
        assert_eq!(
            "CDP:MH",
            format!("{}", CollateralDamagePotential::MediumHigh)
        );
        assert_eq!("CDP:H", format!("{}", CollateralDamagePotential::High));

        // Target distribution
        assert_eq!("TD:N", format!("{}", TargetDistribution::None));
        assert_eq!("TD:L", format!("{}", TargetDistribution::Low));
        assert_eq!("TD:M", format!("{}", TargetDistribution::Medium));
        assert_eq!("TD:H", format!("{}", TargetDistribution::High));

        // Confidentiality Requirement
        assert_eq!("CR:L", format!("{}", ConfidentialityRequirement::Low));
        assert_eq!("CR:M", format!("{}", ConfidentialityRequirement::Medium));
        assert_eq!("CR:H", format!("{}", ConfidentialityRequirement::High));

        // Integrity Requirement
        assert_eq!("IR:L", format!("{}", IntegrityRequirement::Low));
        assert_eq!("IR:M", format!("{}", IntegrityRequirement::Medium));
        assert_eq!("IR:H", format!("{}", IntegrityRequirement::High));

        // Availability Requirement
        assert_eq!("AR:L", format!("{}", AvailabilityRequirement::Low));
        assert_eq!("AR:M", format!("{}", AvailabilityRequirement::Medium));
        assert_eq!("AR:H", format!("{}", AvailabilityRequirement::High));

        // Environmental vector
        assert_eq!(
            "CDP:H/TD:H/CR:M/IR:M/AR:H",
            format!("{}", provide_environmental_vector1())
        );

        // Missing fields
        let mut missing_fields = provide_environmental_vector1();
        missing_fields.collateral_damage_potential = None;
        missing_fields.target_distribution = None;
        assert_eq!("CR:M/IR:M/AR:H", format!("{}", missing_fields));
    }

    #[test]
    fn test_parsing() {
        // Collateral Damage Potential
        assert_eq!(
            Some(CollateralDamagePotential::None),
            CollateralDamagePotential::parse("CDP:N")
        );
        assert_eq!(
            Some(CollateralDamagePotential::Low),
            CollateralDamagePotential::parse("CDP:L")
        );
        assert_eq!(
            Some(CollateralDamagePotential::LowMedium),
            CollateralDamagePotential::parse("CDP:LM")
        );
        assert_eq!(
            Some(CollateralDamagePotential::MediumHigh),
            CollateralDamagePotential::parse("CDP:MH")
        );
        assert_eq!(
            Some(CollateralDamagePotential::High),
            CollateralDamagePotential::parse("CDP:H")
        );
        // Target distribution
        assert_eq!(
            Some(TargetDistribution::None),
            TargetDistribution::parse("TD:N")
        );
        assert_eq!(
            Some(TargetDistribution::Low),
            TargetDistribution::parse("TD:L")
        );
        assert_eq!(
            Some(TargetDistribution::Medium),
            TargetDistribution::parse("TD:M")
        );
        assert_eq!(
            Some(TargetDistribution::High),
            TargetDistribution::parse("TD:H")
        );

        // Confidentiality Requirement
        assert_eq!(
            Some(ConfidentialityRequirement::Low),
            ConfidentialityRequirement::parse("CR:L")
        );
        assert_eq!(
            Some(ConfidentialityRequirement::Medium),
            ConfidentialityRequirement::parse("CR:M")
        );
        assert_eq!(
            Some(ConfidentialityRequirement::High),
            ConfidentialityRequirement::parse("CR:H")
        );

        // Integrity Requirement
        assert_eq!(
            Some(IntegrityRequirement::Low),
            IntegrityRequirement::parse("IR:L")
        );
        assert_eq!(
            Some(IntegrityRequirement::Medium),
            IntegrityRequirement::parse("IR:M")
        );
        assert_eq!(
            Some(IntegrityRequirement::High),
            IntegrityRequirement::parse("IR:H")
        );

        // Availability Requirement
        assert_eq!(
            Some(AvailabilityRequirement::Low),
            AvailabilityRequirement::parse("AR:L")
        );
        assert_eq!(
            Some(AvailabilityRequirement::Medium),
            AvailabilityRequirement::parse("AR:M")
        );
        assert_eq!(
            Some(AvailabilityRequirement::High),
            AvailabilityRequirement::parse("AR:H")
        );

        // Environmental vector
        // Happy path
        assert_eq!(
            Some(provide_environmental_vector1()),
            EnvironmentalVector::parse_optional(&Box::new("CDP:H/TD:H/CR:M/IR:M/AR:H".split('/')))
        );
        // Wrong order
        assert_eq!(
            Some(provide_environmental_vector1()),
            EnvironmentalVector::parse_optional(&Box::new("TD:H/CR:M/CDP:H/IR:M/AR:H".split('/')))
        );
        // Missing fields
        let mut missing_fields = provide_environmental_vector1();
        missing_fields.collateral_damage_potential = None;
        missing_fields.target_distribution = None;
        assert_eq!(
            Some(missing_fields),
            EnvironmentalVector::parse_optional(&Box::new("CR:M/IR:M/AR:H".split('/')))
        );
        // Junk
        assert_eq!(
            None,
            EnvironmentalVector::parse_optional(&mut Box::new("fsjfskhf".split('/')))
        );
        assert_eq!(
            None,
            EnvironmentalVector::parse_optional(&mut Box::new("fs//jf/skhf".split('/')))
        );
    }

    #[test]
    fn test_scoring() {
        let environmental_vector = provide_environmental_vector1();

        assert_eq!(
            9.2,
            environmental_vector.score(provide_base_vector1(), Some(provide_temporal_vector1()))
        );
    }

    // CVE-2002-0392 see CVSS v2 specification section 3.3.1
    // Using the "High" variant
    fn provide_environmental_vector1() -> EnvironmentalVector {
        EnvironmentalVector {
            collateral_damage_potential: Some(CollateralDamagePotential::High),
            target_distribution: Some(TargetDistribution::High),
            confidentiality_requirement: Some(ConfidentialityRequirement::Medium),
            integrity_requirement: Some(IntegrityRequirement::Medium),
            availability_requirement: Some(AvailabilityRequirement::High),
        }
    }

    // CVE-2002-0392 see CVSS v2 specification section 3.3.1
    fn provide_temporal_vector1() -> TemporalVector {
        TemporalVector {
            exploitability: Some(Exploitability::Functional),
            remediation_level: Some(RemediationLevel::OfficialFix),
            report_confidence: Some(ReportConfidence::Confirmed),
        }
    }

    // CVE-2002-0392 see CVSS v2 specification section 3.3.1
    fn provide_base_vector1() -> BaseVector {
        BaseVector {
            access_vector: AccessVector::Network,
            access_complexity: AccessComplexity::Low,
            authentication: Authentication::None,
            confidentiality_impact: ConfidentialityImpact::None,
            integrity_impact: IntegrityImpact::None,
            availability_impact: AvailabilityImpact::Complete,
        }
    }
}
