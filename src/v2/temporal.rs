//! Temporal vector implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// A CVSS V2.0 temporal vector.
///
/// Per the CVSS specification, this structure contains an Exploitability field, a Remediation Level field, and a Report Confidence field, all optionals.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct TemporalVector {
    /// Optional Exploitability field as defined by the CVSS specification.
    pub exploitability: Option<Exploitability>,
    /// Optional Remediation Level field as defined by the CVSS specification.
    pub remediation_level: Option<RemediationLevel>,
    /// Optional Report Confidence field as defined by the CVSS specification.
    pub report_confidence: Option<ReportConfidence>,
}

/// The Exploitability field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.exploitability`] to [`None`].
///
/// [`TemporalVector.exploitability`]: struct.TemporalVector.html#structfield.exploitability
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum Exploitability {
    /// Unproven ("E:U") value for the Exploitability field.
    Unproven,
    /// Proof Of Concept ("E:POC") value for the Exploitability field.
    ProofOfConcept,
    /// Functional ("E:F") value for the Exploitability field.
    Functional,
    /// High ("E:H") value for the Exploitability field.
    High,
    // "Not Defined" is considered the same as a missing value. This is done by setting TemporalVector.exploitability to None.
}

/// The Remediation Level field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.remediation_level`] to [`None`].
///
/// [`TemporalVector.remediation_level`]: struct.TemporalVector.html#structfield.remediation_level
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum RemediationLevel {
    /// Official Fix ("RL:OF") value for the Remediation Level field.
    OfficialFix,
    /// Temporary Fix ("RL:TF") value for the Remediation Level field.
    TemporaryFix,
    /// Workaround ("RL:W") value for the Remediation Level field.
    Workaround,
    /// Unavailable ("RL:U") value for the Remediation Level field.
    Unavailable,
    // "Not Defined" is considered the same as a missing value. This is done by setting TemporalVector.remediation_level to None.
}

/// The Report Confidence field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.report_confidence`] to [`None`].
///
/// [`TemporalVector.report_confidence`]: struct.TemporalVector.html#structfield.report_confidence
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ReportConfidence {
    /// Unconfirmed ("RC:UC") value for the Report Confidence field.
    Unconfirmed,
    /// Uncorroborated ("RC:UR") value for the Report Confidence field.
    Uncorroborated,
    /// Confirmed ("RC:C") value for the Report Confidence field.
    Confirmed,
    // "Not Defined" is considered the same as a missing value. This is done by setting TemporalVector.remediation_level to None.
}

impl TemporalVector {
    /// Provides the severity score for the CVSS temporal vector, given a base score as input.
    ///
    /// Calling this method is identical to calling [`CVSS2Vector.score()`] when [`CVSS2Vector.environmental`] is set to [`None`].
    ///
    /// [`CVSS2Vector.score()`]: ../struct.CVSS2Vector.html#method.score
    /// [`CVSS2Vector.environmental`]: ../struct.CVSS2Vector.html#structfield.environmental
    /// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
    pub fn score(&self, base_score: f64) -> f64 {
        let mut result = base_score;

        result = match self.exploitability {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        result = match self.remediation_level {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        result = match self.report_confidence {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        (result * 10.0).round() / 10.0
    }
}

impl OptionalParser for TemporalVector {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = TemporalVector {
            exploitability: Exploitability::parse_optional(split),
            remediation_level: RemediationLevel::parse_optional(split),
            report_confidence: ReportConfidence::parse_optional(split),
        };

        if res.exploitability.is_none()
            && res.remediation_level.is_none()
            && res.report_confidence.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl Exploitability {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            Exploitability::Unproven => 0.85,
            Exploitability::ProofOfConcept => 0.9,
            Exploitability::Functional => 0.95,
            Exploitability::High => 1.0,
        }
    }
}

impl RemediationLevel {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            RemediationLevel::OfficialFix => 0.87,
            RemediationLevel::TemporaryFix => 0.9,
            RemediationLevel::Workaround => 0.95,
            RemediationLevel::Unavailable => 1.0,
        }
    }
}

impl ReportConfidence {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ReportConfidence::Unconfirmed => 0.9,
            ReportConfidence::Uncorroborated => 0.95,
            ReportConfidence::Confirmed => 1.0,
        }
    }
}

impl Field for Exploitability {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Exploitability::Unproven => "E:U",
            Exploitability::ProofOfConcept => "E:POC",
            Exploitability::Functional => "E:F",
            Exploitability::High => "E:H",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<Exploitability> {
        if input == Exploitability::High.abbreviated_form() {
            Some(Exploitability::High)
        } else if input == Exploitability::Functional.abbreviated_form() {
            Some(Exploitability::Functional)
        } else if input == Exploitability::ProofOfConcept.abbreviated_form() {
            Some(Exploitability::ProofOfConcept)
        } else if input == Exploitability::Unproven.abbreviated_form() {
            Some(Exploitability::Unproven)
        } else {
            None
        }
    }
}

impl Field for RemediationLevel {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            RemediationLevel::OfficialFix => "RL:OF",
            RemediationLevel::TemporaryFix => "RL:TF",
            RemediationLevel::Workaround => "RL:W",
            RemediationLevel::Unavailable => "RL:U",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<RemediationLevel> {
        if input == RemediationLevel::OfficialFix.abbreviated_form() {
            Some(RemediationLevel::OfficialFix)
        } else if input == RemediationLevel::TemporaryFix.abbreviated_form() {
            Some(RemediationLevel::TemporaryFix)
        } else if input == RemediationLevel::Workaround.abbreviated_form() {
            Some(RemediationLevel::Workaround)
        } else if input == RemediationLevel::Unavailable.abbreviated_form() {
            Some(RemediationLevel::Unavailable)
        } else {
            None
        }
    }
}

impl Field for ReportConfidence {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ReportConfidence::Unconfirmed => "RC:UC",
            ReportConfidence::Uncorroborated => "RC:UR",
            ReportConfidence::Confirmed => "RC:C",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ReportConfidence> {
        if input == ReportConfidence::Unconfirmed.abbreviated_form() {
            Some(ReportConfidence::Unconfirmed)
        } else if input == ReportConfidence::Uncorroborated.abbreviated_form() {
            Some(ReportConfidence::Uncorroborated)
        } else if input == ReportConfidence::Confirmed.abbreviated_form() {
            Some(ReportConfidence::Confirmed)
        } else {
            None
        }
    }
}

impl OptionalParser for Exploitability {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for RemediationLevel {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for ReportConfidence {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for Exploitability {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for RemediationLevel {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ReportConfidence {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for TemporalVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.exploitability {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.remediation_level {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.report_confidence {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        // Exploitability
        assert_eq!("E:U", format!("{}", Exploitability::Unproven));
        assert_eq!("E:POC", format!("{}", Exploitability::ProofOfConcept));
        assert_eq!("E:F", format!("{}", Exploitability::Functional));
        assert_eq!("E:H", format!("{}", Exploitability::High));

        // Remediation Level
        assert_eq!("RL:OF", format!("{}", RemediationLevel::OfficialFix));
        assert_eq!("RL:TF", format!("{}", RemediationLevel::TemporaryFix));
        assert_eq!("RL:W", format!("{}", RemediationLevel::Workaround));
        assert_eq!("RL:U", format!("{}", RemediationLevel::Unavailable));

        // Report Confidence
        assert_eq!("RC:UC", format!("{}", ReportConfidence::Unconfirmed));
        assert_eq!("RC:UR", format!("{}", ReportConfidence::Uncorroborated));
        assert_eq!("RC:C", format!("{}", ReportConfidence::Confirmed));

        // Temporal vector
        assert_eq!("E:F/RL:OF/RC:C", format!("{}", provide_temporal_vector1()));

        // Missing fields
        let mut missing_fields = provide_temporal_vector1();
        missing_fields.exploitability = None;
        assert_eq!("RL:OF/RC:C", format!("{}", missing_fields));
    }

    #[test]
    fn test_parsing() {
        // Exploitability
        assert_eq!(Some(Exploitability::Unproven), Exploitability::parse("E:U"));
        assert_eq!(
            Some(Exploitability::ProofOfConcept),
            Exploitability::parse("E:POC")
        );
        assert_eq!(
            Some(Exploitability::Functional),
            Exploitability::parse("E:F")
        );
        assert_eq!(Some(Exploitability::High), Exploitability::parse("E:H"));

        // Remediation Level
        assert_eq!(
            Some(RemediationLevel::OfficialFix),
            RemediationLevel::parse("RL:OF")
        );
        assert_eq!(
            Some(RemediationLevel::TemporaryFix),
            RemediationLevel::parse("RL:TF")
        );
        assert_eq!(
            Some(RemediationLevel::Workaround),
            RemediationLevel::parse("RL:W")
        );
        assert_eq!(
            Some(RemediationLevel::Unavailable),
            RemediationLevel::parse("RL:U")
        );

        // Report confidence
        assert_eq!(
            Some(ReportConfidence::Unconfirmed),
            ReportConfidence::parse("RC:UC")
        );
        assert_eq!(
            Some(ReportConfidence::Uncorroborated),
            ReportConfidence::parse("RC:UR")
        );
        assert_eq!(
            Some(ReportConfidence::Confirmed),
            ReportConfidence::parse("RC:C")
        );

        // Temporal Vector
        // Happy path
        assert_eq!(
            Some(provide_temporal_vector1()),
            TemporalVector::parse_optional(&Box::new("E:F/RL:OF/RC:C".split('/')))
        );
        // Wrong order
        assert_eq!(
            Some(provide_temporal_vector1()),
            TemporalVector::parse_optional(&Box::new("RL:OF/E:F/RC:C".split('/')))
        );
        // Missing fields
        let mut missing_fields = provide_temporal_vector1();
        missing_fields.remediation_level = None;
        assert_eq!(
            Some(missing_fields),
            TemporalVector::parse_optional(&Box::new("E:F/RC:C".split('/')))
        );
        // Junk
        assert_eq!(
            None,
            TemporalVector::parse_optional(&mut Box::new("fsjfskhf".split('/'))),
        );
        assert_eq!(
            None,
            TemporalVector::parse_optional(&mut Box::new("fs//jf/skhf".split('/'))),
        );
    }

    #[test]
    fn test_scoring() {
        assert_eq!(6.4, provide_temporal_vector1().score(7.8));
    }

    // CVE-2002-0392 see CVSS v2 specification section 3.3.1
    fn provide_temporal_vector1() -> TemporalVector {
        TemporalVector {
            exploitability: Some(Exploitability::Functional),
            remediation_level: Some(RemediationLevel::OfficialFix),
            report_confidence: Some(ReportConfidence::Confirmed),
        }
    }
}
