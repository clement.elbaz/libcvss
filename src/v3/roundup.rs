//! Roundup implementation.
//!
//! This is a roundup function implementation, as defined by CVSS 3.0 Specification.

/// “Round up” is defined as the smallest number, specified to one decimal place, that is
/// equal to or higher than its input. For example, Round up (4.02) is 4.1; and Round up (4.00) is 4.0.
pub fn roundup(input: f64) -> f64 {
    let x10 = input * 10.0;
    let frac_part = x10.fract();
    let mut int_part = x10.trunc();
    if frac_part > 0.0 {
        int_part += 1.0;
    }
    int_part / 10.0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_roundup() {
        assert_eq!(4.1, roundup(4.02));

        assert_eq!(4.00, roundup(4.0));

        assert_eq!(0.4, roundup(0.1 + 0.2));

        assert_eq!(0.4, roundup(0.3000001));

        assert_eq!(0.4, roundup(0.30001));
    }
}
