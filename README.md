# What is libcvss ?
libcvss is a Rust implementation of the CVSS specification.

It provides Rust users with a native way to manipulate CVSS-formatted vulnerability data.

libcvss leverages Rust to provide a CVSS implementation focused on both performance and correctness.
# What is CVSS ?
[The official CVSS website](https://www.first.org/cvss/) describes CVSS this way:
> The Common Vulnerability Scoring System (CVSS) provides a way to capture the principal characteristics of a vulnerability and produce a numerical score reflecting its severity. The numerical score can then be translated into a qualitative representation (such as low, medium, high, and critical) to help organizations properly assess and prioritize their vulnerability management processes.
# Features
- All non-deprecated versions of CVSS (2.0, 3.0, 3.1) are supported.
- CVSS vectors can be parsed from a string slice, or constructed programmatically.
- Parsing can be done in strict or non-strict mode : strict mode requires all fields inside the vector string to be present in canonical order. In non-strict mode the parser will do its best to construct a CVSS vector from what it can find in the input: fields can be repeated, in the wrong order, etc.
- Score associated with a CVSS vector can be computed for base, temporal or environmental vectors.
- libcvss can parse thousands of CVSS vectors per second.
# Test infrastructure and reliability
Correctness and reliability are an important focus of libcvss.
- All official examples from all versions of the CVSS specification are implemented as integration tests. We pledge to have these tests pass on master at all time.
- An optional integration test downloads the NVD data for every CVE vulnerability disclosed since 2007, parses the associated CVSS vectors (in both V2 and V3 version when available), computes the associated severity scores and compares it to the scores provided by NVD. We pledge to have this test pass on all libcvss releases.
# How to contribute
libcvss is released under a BSD-3 license.

Bug reports and pull requests are appreciated. Please use [Inria's Gitlab](https://gitlab.inria.fr/celbaz/libcvss) for both.

Before submitting a pull request, please consider using our pre-commit hook.
You can activate it using:
```shell script
ln -s hooks/pre-commit .git/hooks/pre-commit
```
Our pre-commit hook will check that:
- Code is compiling properly.
- Tests are passing.
- Code is properly formatted (using rustfmt).

If your pull request is substantial, consider launching our integration tests suite.
 These tests download past vulnerability data from the NVD database and perform parsing against real vulnerability metadata. It requires an Internet connection and the availability of the following executables: *sh*, *wget*, *gunzip*, and *jq*:
```shell script
cargo test -- --ignored --nocapture
```

Thank you for your interest in libcvss.